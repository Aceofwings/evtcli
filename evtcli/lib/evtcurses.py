import curses
from threading import Lock
import time
class ScreenObject(object):
    x = None
    y = None
    colorPair = 0
    text = "None"
    _data = 0
    @property
    def data(self):
        return str(self._data)

    @data.setter
    def data(self,value):
        self._data = value

    def __init__(self,x,y,text):
        self.x = x
        self.y = y
        self.text = text


screenObjectsLock = Lock()
screenObjects = []
ORANGE = 209
DARK = 17



def appendToScreenObjects(screenObject):
    with screenObjectsLock:
        screenObjects.append(screenObject)

def startCurses(running):
    curses.wrapper(text_screen, running)

def text_screen(stdscr,running):
    curses.use_default_colors()
    for i in range(0, curses.COLORS):
        curses.init_pair(i + 1, i, -1)
    while not running.isSet():
        stdscr.clear()
        with screenObjectsLock:
            for so in screenObjects:
                stdscr.addstr(so.y,so.x, so.text + so.data,curses.color_pair(so.colorPair))
        stdscr.refresh()
        time.sleep(.1)


        # stdscr.clear()
        # stdscr.addstr(0, 0, 'Hello This is a console screen')
        # stdscr.addstr(1, 5, 'Discharge 23: %d'%(self.discharge23))
